package com.polytechtours.aytakine.IntegrationPile;

import java.util.ArrayList;
import java.util.Observable;

class Pile extends Observable
{
    private ArrayList<Integer> elements;

    public Pile()
    {
        elements = new ArrayList<>();
    }

    public int getSizeList()
    {
        System.out.println("Getting PILE SIZE");
        return elements.size();
    }

    public int getEntier(int index)
    {
        System.out.println("Getting element at " + index);
        return 0;
    }

    public ArrayList<Integer> getElements() {
		return elements;
	}


	public void push(int entier)
    {
        System.out.println("Element " + entier + " added !");
        elements.add(entier);
    }

    public int pop()
    {
        try {
            int e = elements.get(elements.size()-1);
            elements.remove(elements.size()-1);
            return e;
        }catch(IndexOutOfBoundsException e)
        {
            throw e;
        }
    }

    public void clear()
    {
    	elements.clear();
        System.out.println("Pile cleared!");
    }
}
