package com.polytechtours.aytakine.IntegrationPile;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

public class ViewTopPile implements Observer
{
	static JLabel sommetValeur;
	static JFrame frame;

	public ViewTopPile(InputPileStrategy pileObservable)
	{
		pileObservable.addObserver(this);
		frame = new JFrame("ViewTopPile");
		sommetValeur = new JLabel("-");
		JPanel p = new JPanel();
		p.add(sommetValeur);
		frame.add(p);
		frame.setSize(300, 200);
		frame.show();
	}

	@Override
	public void update(Observable o, Object arg)
	{
		Pile pile = ((Pile) arg);
		try
		{
			int result = pile.getElements().get(pile.getSizeList() - 1);
			sommetValeur.setText(""+result);
		}catch(ArrayIndexOutOfBoundsException e)
		{
			sommetValeur.setText("Sommet: vide");
		}

	}
}
