package com.polytechtours.aytakine.IntegrationPile;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ViewInputPile extends InputPileStrategy
{
    static JTextField textField;
    static JFrame frame;
    static JButton button;
    static JLabel label;

    static JButton pop;
    static JButton clear;

    class text extends JFrame implements ActionListener
    {
        ViewInputPile viewMain;
        text(ViewInputPile viewInputPile)
        {
            viewMain = viewInputPile;
        }
        @Override
        public void actionPerformed(ActionEvent actionEvent)
        {
            System.out.println("CALLED");
            String number = textField.getText();
            try
            {
                viewMain.push(Integer.parseInt(number));
                System.out.println("pushed: " + number);
            }
            catch(NumberFormatException e)
            {
                label.setText("Valeur incorrect !");
            }
            textField.setText("");
        }
    }

    class popIt implements ActionListener
    {
        ViewInputPile viewMain;
        popIt(ViewInputPile viewInputPile)
        {
            viewMain = viewInputPile;
        }
        @Override
        public void actionPerformed(ActionEvent actionEvent)
        {
            try {
                int number = viewMain.pop();
                label.setText("Element popped: " + number);
            }catch(IndexOutOfBoundsException e)
            {
                label.setText("No element to pop");
            }
        }
    }

    class cleanIt implements ActionListener
    {
        ViewInputPile viewMain;
        cleanIt(ViewInputPile viewInputPile)
        {
            viewMain = viewInputPile;
        }
        @Override
        public void actionPerformed(ActionEvent actionEvent)
        {
            viewMain.clear();
            label.setText("cleared");
        }
    }

    public ViewInputPile()
    {
        frame = new JFrame("ViewInputPile");
        label = new JLabel("Saisissez votre valeur");
        button = new JButton("Envoi");

        popIt popAction = new popIt(this);
        pop = new JButton("Retirer le sommet");
        pop.addActionListener(popAction);

        cleanIt cleanAction = new cleanIt(this);
        clear = new JButton("Clear");
        clear.addActionListener(cleanAction);

        text te = new text(this);
        button.addActionListener(te);
        textField = new JTextField(16);
        JPanel p = new JPanel();
        p.add(textField);
        p.add(button);
        p.add(label);

        p.add(pop);
        p.add(clear);

        frame.add(p);

        frame.setSize(300, 200);
        frame.show();
    }

    @Override
    void actionCommande()
    {
        System.out.println("ViewInputPile actioncommande");
    }
}
