package com.polytechtours.aytakine.IntegrationPile;

public class Scenario2 {

    public static void main(String[] args)
    {
        ViewInputPile viewInputPile = new ViewInputPile();
        ViewBottomPile viewBottomPile = new ViewBottomPile(viewInputPile);
        ViewTopPile viewTopPile = new ViewTopPile(viewInputPile);
    }
}
