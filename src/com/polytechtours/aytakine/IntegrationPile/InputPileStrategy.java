package com.polytechtours.aytakine.IntegrationPile;


import java.awt.event.ActionEvent;
import java.util.Observable;

public abstract class InputPileStrategy extends Observable
{

    Pile pile;

    public InputPileStrategy()
    {
        pile = new Pile();
    }


    public void push(int entier) {
        System.out.println("InputPileStrategy");
        pile.push(entier);
        setChanged();
        notifyObservers(pile);
    }


    public int pop() {
        System.out.println("InputPileStrategy");
        int element = pile.pop();
        setChanged();
        notifyObservers(pile);
        return element;
    }


    public void clear() {
        System.out.println("InputPileStrategy");
        pile.clear();
        setChanged();
        notifyObservers(pile);
    }

    abstract void actionCommande();
}
