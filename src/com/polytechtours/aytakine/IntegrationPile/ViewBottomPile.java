package com.polytechtours.aytakine.IntegrationPile;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

public class ViewBottomPile implements Observer
{

	static JLabel elementOne, elementTwo, elementThree, elementFour, elementFive;

	static JFrame frame;

	public ViewBottomPile(InputPileStrategy pileObservable) {
		pileObservable.addObserver(this);
		frame = new JFrame("ViewBottomPile");
		elementOne = new JLabel("Element 4: -");
		elementTwo = new JLabel("Element 3: -");
		elementThree = new JLabel("Element 2: -");
		elementFour = new JLabel("Element 1: -");
		elementFive = new JLabel("Element 0: -");
		JPanel p = new JPanel();
		p.add(elementOne);
		p.add(elementTwo);
		p.add(elementThree);
		p.add(elementFour);
		p.add(elementFive);
		frame.add(p);
		frame.setSize(300, 200);
		frame.show();
	}

	@Override
	public void update(Observable o, Object arg) {
		Pile pile = ((Pile) arg);

		try {
			elementOne.setText("Element 4: " + pile.getElements().get(4));
		}catch (IndexOutOfBoundsException e)
		{
			elementOne.setText("Element 4: -");
		}

		try {
			elementTwo.setText("Element 3: "+pile.getElements().get(3));
		}catch (IndexOutOfBoundsException e)
		{
			elementTwo.setText("Element 3: -");
		}

		try {
			elementThree.setText("Element 2: "+pile.getElements().get(2));
		}catch (IndexOutOfBoundsException e)
		{
			elementThree.setText("Element 2: -");
		}

		try {
			elementFour.setText("Element 1: "+pile.getElements().get(1));
		}catch (IndexOutOfBoundsException e)
		{
			elementFour.setText("Element 1: -");
		}

		try {
			elementFive.setText("Element 0: " +pile.getElements().get(0));
		}catch (IndexOutOfBoundsException e)
		{
			elementFive.setText("Element 0: -");
		}
	}

}

